# Switch Saturation and Value for Paint.NET

Switches the saturation and value channels of the selection

[`Switch Saturation and Value.cs`](Switch%20Saturation%20and%20Value.cs) is the source code  
[`Switch Saturation and Value.dll`](Switch%20Saturation%20and%20Value.dll) is the compiled .dll  
[`logo.png`](logo.png) is the effect icon

**INSTALLATION INSTRUCTIONS:**  
You only need to download [`Switch Saturation and Value.dll`](Switch%20Saturation%20and%20Value.dll) and place it in `C:\Program Files\paint.net\Effects`

Created with CodeLab: https://boltbait.com/pdn/codelab/

Paint.NET: https://www.getpaint.net/

## Sample Images

Click any of these to enlarge; they're much bigger than shown here.

Original image:  
<img src="samples/orig.png" alt="orig" width=150>

Isolating the value channel with [Isolate Channels for Paint.NET](../../../../pdn-flag-rippler):  
<img src="samples/value.png" alt="value" width=150>

The original image after running Switch Saturation and Value:  
<img src="samples/switched.png" alt="switched" width=150>

After switching, isolating the saturation channel:  
<img src="samples/switched saturation.png" alt="switched saturation" width=150>
